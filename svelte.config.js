import preprocess from "svelte-preprocess"
import adapter from "@sveltejs/adapter-static"

const projectPathPrefix =
  process.env.CI_PROJECT_NAME || "minisite-generator"

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: [preprocess({
        "postcss": true
    })],

	kit: {
		adapter: adapter(),
		paths: {
			base: `/${projectPathPrefix}`
		},
		// hydrate the <div id="svelte"> element in src/app.html
		target: "#svelte",
	},
}

export default config
// Workaround until SvelteKit uses Vite 2.3.8 (and it's confirmed to fix the Tailwind JIT problem)
const mode = process.env.NODE_ENV;
const dev = mode === "development";
process.env.TAILWIND_MODE = dev ? "watch" : "build";
