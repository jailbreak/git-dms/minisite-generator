import type { RequestHandler } from "@sveltejs/kit"
import { Gitlab } from "@gitbeaker/node"

export const get: RequestHandler = async ({ locals, query }) => {
    const projectId = query.get("project")
    const ref = query.get("ref")

    const gitlabClient = new Gitlab({ host: locals.config.gitlabUrl })
    const { RepositoryFiles } = gitlabClient

    let datapackageRaw
    try {
        datapackageRaw = await RepositoryFiles.showRaw(projectId, "datapackage.json", ref)
    } catch (error) {
        return {
            status: 404,
            body: { error: "Could not find 'datapackage.json' in GitLab project" },
        }
    }

    return { body: datapackageRaw }
}
