function isNonEmptyString(v: unknown): boolean {
    if (v === null || v === undefined) {
        return false
    }
    if (typeof v === "string") {
        return v.length > 0
    }
    return false
}

export interface Config {
	adminUiUrl: string
	gitlabUrl: string
	projectId: string
}

export function loadConfigFromEnv() {
    const projectId = import.meta.env.VITE_PROJECT_ID
    if (!isNonEmptyString(projectId)) {
        throw new Error(`VITE_PROJECT_ID environment variable must be a non-empty string`)
    }

    return {
        adminUiUrl: import.meta.env.VITE_ADMIN_UI_URL || "https://git-dms.netlify.app",
        gitlabUrl: import.meta.env.VITE_GITLAB_URL || "https://gitlab.com",
        projectId
    }
}
