import type { GetSession, Handle } from "@sveltejs/kit"
import type { Config } from "./config"
import { loadConfigFromEnv } from "./config"

interface MyLocals {
	config: Config
}

interface MySession {
	config: Config
}

export const handle: Handle<MyLocals> = async ({ request, resolve }) => {
	request.locals.config = loadConfigFromEnv()

	const response = await resolve(request)

	return response
}

export const getSession: GetSession<MyLocals, MySession> = (request) => {
	return {
		config: request.locals?.config,
	}
}
