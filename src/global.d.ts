/// <reference types="@sveltejs/kit" />
/// <reference types="svelte" />
/// <reference types="vite/client" />

interface ImportMetaEnv {
    VITE_ADMIN_UI_URL: string
    VITE_GITLAB_URL: string
    VITE_PROJECT_ID: string
}
