# Minisite generator

This project is part of Git DMS (Data Management System). It's a static site generator for data packages.

It can be run from a GitLab CI pipeline. This project provides a pipeline ready to be included: [`ci-pipeline/build-minisite.yml`](ci-pipeline/build-minisite.yml).

## Development

This application can be tested locally:

```bash
cp .env.template .env
# Fill the values in .env
npm run dev
```

### Configuration

- `VITE_ADMIN_UI_URL`: the URL to Git DMS admin UI
- `VITE_PROJECT_ID`: GitLab project ID, as it appears in the URL, for example `{username}/{project}`

Note: the GitLab project must be public so that the `datapackage.json` file can be read.

### Test container locally

The minisite-generator is executed by [this CI pipeline](./ci-pipeline/build-minisite.yml) that is included by Git repositories containing data packages. This pipeline uses a container image.

To test this image locally, first you need to build it, then run it.

To build the container image locally:

```bash
docker build -t minisite-generator:latest .
```

Note: the minisite-generator works by reading the files of the GitLab project corresponding to the env variable `VITE_PROJECT_ID` remotely, not by reading files in a local directory.

To run the container image locally:

```bash
mkdir container-build
docker run --env VITE_PROJECT_ID=mancelin/demo-dms -v $PWD/container-build:/app/build minisite-generator:latest
```

The build result will be available in the local `container-build` directory. To serve files in order to check the result, use a tool like [sirv-cli](https://www.npmjs.com/package/sirv-cli):

```bash
npx sirv-cli container-build
```

Then open the displayed URL in your browser (<http://localhost:5000>).

## Notes

Currently this project reads data packages from GitLab projects, but we plan to support more storage backends.
