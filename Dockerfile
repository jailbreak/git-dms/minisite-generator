FROM node:14

WORKDIR /app

COPY package*.json ./
# Also install dev dependencies
RUN npm install

COPY . .

CMD ["npm", "run", "build"]
